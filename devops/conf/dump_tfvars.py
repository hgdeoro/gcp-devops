import sys
import os
import re
import logging

REGEX = re.compile(r'^'
                   r'([a-zA-Z0-9_-]+)'
                   r'\s*'
                   r'='
                   r'\s*'
                   r'[\'\"](.*)[\'\"]'
                   r'$')


class Main():

    @property
    def tf_filename(self):
        return os.environ['TF_FILE']

    @property
    def tfvars_path(self):
        current_dir_path = os.path.dirname(os.path.abspath(__file__))
        tfvars_file_path = os.path.join(current_dir_path, self.tf_filename)
        assert os.path.exists(tfvars_file_path)
        return tfvars_file_path

    def read_lines(self):
        with open(self.tfvars_path, 'r') as tfvars:
            for line in tfvars.readlines():
                yield line.strip()

    def valid_lines(self):
        for line in self.read_lines():
            if not line:
                continue
            if line.startswith('#') or line.startswith('//'):
                continue
            match = REGEX.fullmatch(line)
            if not match:
                logging.warning("Invalid line: %s", line)
                continue
            yield match.groups()

    def main(self, vars):
        for key, value in self.valid_lines():
            if not vars or key in vars:
                print(f"{key}={value}")


if __name__ == '__main__':
    logging.basicConfig(level=logging.WARNING)
    Main().main(sys.argv[1:])
