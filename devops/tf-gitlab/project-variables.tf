//
// Project -> frontend
//

resource "gitlab_project_variable" "CLOUDSDK_CORE_PROJECT_frontend" {
  project = gitlab_project.frontend.id
  key = "CLOUDSDK_CORE_PROJECT"
  value = var.gcp_project_id
  protected = false
  masked = false
  environment_scope = "*"
}

resource "gitlab_project_variable" "GITLAB_DEPLOYER_AUTHFILE_JSON_frontend" {
  project = gitlab_project.frontend.id
  key = "GITLAB_DEPLOYER_AUTHFILE_JSON"
  value = filebase64("/secrets/gitlab-deployer-authfile.json")
  protected = true
  masked = true
  environment_scope = "*"
}

//
// Project -> backend
//

resource "gitlab_project_variable" "CLOUDSDK_CORE_PROJECT_backend" {
  project = gitlab_project.backend.id
  key = "CLOUDSDK_CORE_PROJECT"
  value = var.gcp_project_id
  protected = false
  masked = false
  environment_scope = "*"
}

resource "gitlab_project_variable" "GITLAB_DEPLOYER_AUTHFILE_JSON_backend" {
  project = gitlab_project.backend.id
  key = "GITLAB_DEPLOYER_AUTHFILE_JSON"
  value = filebase64("/secrets/gitlab-deployer-authfile.json")
  protected = true
  masked = true
  environment_scope = "*"
}
