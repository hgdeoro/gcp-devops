provider "gitlab" {
  version = "~> 2.6"
  token = file("/secrets/gitlab-personal-access-token.txt")
}

terraform {
  backend "gcs" {
    # partial config
    prefix = "terraform/state/gitlab"
    credentials = "/secrets/tf-devops-tf-authfile.json"
  }
}

resource "gitlab_group" "group" {
  name = var.gcp_project_id
  path = var.gcp_project_id
}

//
// Project -> devops
//

resource "gitlab_project" "devops" {
  name = "${var.gcp_project_id}-devops"
  description = "DevOps for ${var.gcp_project_id}"
  namespace_id = gitlab_group.group.id
  default_branch = "master"
}

resource "gitlab_branch_protection" "devops_protect_master" {
  project = gitlab_project.devops.id
  branch = "master"
  push_access_level = "maintainer"
  merge_access_level = "maintainer"
}

resource "gitlab_branch_protection" "devops_protect_acceptance" {
  project = gitlab_project.devops.id
  branch = "acceptance"
  push_access_level = "maintainer"
  merge_access_level = "maintainer"
}

resource "gitlab_branch_protection" "devops_protect_dev" {
  project = gitlab_project.devops.id
  branch = "dev"
  push_access_level = "developer"
  merge_access_level = "developer"
}

//
// Project -> frontend
//

resource "gitlab_project" "frontend" {
  name = "${var.gcp_project_id}-frontend"
  description = "FrontEnd for ${var.gcp_project_id}"
  namespace_id = gitlab_group.group.id
  default_branch = "master"
}

resource "gitlab_branch_protection" "frontend_protect_master" {
  project = gitlab_project.frontend.id
  branch = "master"
  push_access_level = "maintainer"
  merge_access_level = "maintainer"
}

resource "gitlab_branch_protection" "frontend_protect_acceptance" {
  project = gitlab_project.frontend.id
  branch = "acceptance"
  push_access_level = "maintainer"
  merge_access_level = "maintainer"
}

resource "gitlab_branch_protection" "frontend_protect_dev" {
  project = gitlab_project.frontend.id
  branch = "dev"
  push_access_level = "developer"
  merge_access_level = "developer"
}

//
// Project -> backend
//

resource "gitlab_project" "backend" {
  name = "${var.gcp_project_id}-backend"
  description = "BackEnd for ${var.gcp_project_id}"
  namespace_id = gitlab_group.group.id
  default_branch = "master"
}

resource "gitlab_branch_protection" "backend_protect_master" {
  project = gitlab_project.backend.id
  branch = "master"
  push_access_level = "maintainer"
  merge_access_level = "maintainer"
}

resource "gitlab_branch_protection" "backend_protect_acceptance" {
  project = gitlab_project.backend.id
  branch = "acceptance"
  push_access_level = "maintainer"
  merge_access_level = "maintainer"
}

resource "gitlab_branch_protection" "backend_protect_dev" {
  project = gitlab_project.backend.id
  branch = "dev"
  push_access_level = "developer"
  merge_access_level = "developer"
}
