output "gitlab_project_devops" {
  description = "GitLab project / devops"
  value = gitlab_project.devops
}

output "gitlab_project_frontend" {
  description = "GitLab project / frontend"
  value = gitlab_project.frontend
}

output "gitlab_project_backend" {
  description = "GitLab project / backend"
  value = gitlab_project.backend
}
