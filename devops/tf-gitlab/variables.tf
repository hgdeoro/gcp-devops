variable "project_id" {}
variable "project_name" {}
variable "org_id" {}
variable "billing_account" {}
variable "tf_state_bucket_name" {}
variable "tf_state_bucket_location" {}
