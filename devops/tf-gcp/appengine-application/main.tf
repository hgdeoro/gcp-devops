provider "google" {
  project = var.project_id
  version = "~> 3.16"
}

terraform {
  backend "gcs" {
    # partial config
    prefix = "terraform/state/appengine-application"
    credentials = "/secrets/tf-devops-tf-authfile.json"
  }
}

// Method apps.create requires appengine.applications.create on the requested Application resource.
// Also requires "Owner role" permissions on Cloud project.
// https://cloud.google.com/appengine/docs/admin-api/access-control
resource "google_app_engine_application" "app" {
  project = var.project_id
  # TD: we should get 'location_id' from configuration
  location_id = "us-central"
}

resource "google_project_service" "enable_cloudbuild_googleapis" {
  project = var.project_id
  service = "cloudbuild.googleapis.com"
}
