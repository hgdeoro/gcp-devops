provider "google" {
  project = var.project_id
  version = "~> 3.16"
}

terraform {
  backend "gcs" {
    # partial config
    prefix = "terraform/state/appengine"
  }
}

resource "google_service_account" "gitlab_deployer" {
  account_id = "gitlab-deployer"
  display_name = "Service Account to deploy to AppEngine from GitLab"
}

resource "google_project_iam_member" "gitlab_deployer_has_role_appengine_deployer" {
  project = var.project_id
  member = "serviceAccount:${google_service_account.gitlab_deployer.email}"
  role = "roles/appengine.deployer"
}

resource "google_project_iam_member" "gitlab_deployer_has_role_compute_storage_admin" {
  project = var.project_id
  member = "serviceAccount:${google_service_account.gitlab_deployer.email}"
  role = "roles/compute.storageAdmin"
}

resource "google_project_iam_member" "gitlab_deployer_has_role_cloudbuild_builds_editor" {
  project = var.project_id
  member = "serviceAccount:${google_service_account.gitlab_deployer.email}"
  role = "roles/cloudbuild.builds.editor"
}

resource "google_project_iam_member" "gitlab_deployer_has_role_cloudbuild_builds_builder" {
  project = var.project_id
  member = "serviceAccount:${google_service_account.gitlab_deployer.email}"
  role = "roles/cloudbuild.builds.builder"
}

resource "google_project_iam_member" "gitlab_deployer_has_role_appengine_service_admin" {
  project = var.project_id
  member = "serviceAccount:${google_service_account.gitlab_deployer.email}"
  role = "roles/appengine.serviceAdmin"
}

//resource "google_project_iam_member" "gitlab_deployer_has_role_editor_tmp" {
//  project = var.project_id
//  member = "serviceAccount:${google_service_account.gitlab_deployer.email}"
//  role = "roles/editor"
//}

resource "google_service_account_key" "gitlab_deployer" {
  service_account_id = google_service_account.gitlab_deployer.name
}

resource "local_file" "gitlab_deployer_key" {
  filename = "/secrets/gitlab-deployer-authfile.json"
  sensitive_content = base64decode(google_service_account_key.gitlab_deployer.private_key)
  file_permission = "0400"
}
