provider "google" {
  project = var.project_id
  version = "~> 3.16"
}

terraform {
  backend "gcs" {
    # partial config
    prefix = "terraform/state/network"
  }
}

module "network" {
  source = "terraform-google-modules/network/google"
  version = "2.2.0"

  project_id = var.project_id
  network_name = "prod"
  routing_mode = "GLOBAL"
  subnets = [
    {
      subnet_name = "subnet-dev"
      subnet_ip = "10.1.1.0/24"
      subnet_region = "us-west1"
      subnet_private_access = "true"
    },
    {
      subnet_name = "subnet-prod"
      subnet_ip = "10.1.99.0/24"
      subnet_region = "us-west1"
      subnet_private_access = "true"
    },
  ]
}
