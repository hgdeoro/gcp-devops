provider "google" {
  version = "~> 3.16"
}

provider "local" {
  version = "~> 1.4"
}

resource "google_project" "project" {
  project_id = var.project_id
  name = var.project_name
  org_id = var.org_id
  billing_account = var.billing_account
}

resource "google_service_account" "tf_superuser" {
  project = var.project_id
  account_id = "tf-superuser"
  display_name = "Service Account SuperUser for Terraform"
}

resource "google_service_account_key" "tf_superuser_key" {
  service_account_id = google_service_account.tf_superuser.name
}

resource "local_file" "tf_superuser_key" {
  filename = "/secrets/tf-superuser-${var.project_id}.json"
  sensitive_content = base64decode(google_service_account_key.tf_superuser_key.private_key)
  file_permission = "0400"
}
