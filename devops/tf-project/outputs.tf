output "project" {
  value = google_project.project
}

output "service_account_tf_superuser" {
  value = google_service_account.tf_superuser
}
