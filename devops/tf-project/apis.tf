resource "google_project_service" "enable_googleapis_compute" {
  project = var.project_id
  service = "compute.googleapis.com"
}

resource "google_project_service" "enable_googleapis_appengine" {
  project = var.project_id
  service = "appengine.googleapis.com"
}

resource "google_project_service" "enable_googleapis_iam" {
  project = var.project_id
  service = "iam.googleapis.com"
}

resource "google_project_service" "cloudresourcemanager" {
  project = var.project_id
  service = "cloudresourcemanager.googleapis.com"
}
resource "google_project_service" "serviceusage" {
  project = var.project_id
  service = "serviceusage.googleapis.com"
}
