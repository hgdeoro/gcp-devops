resource "google_project_iam_member" "tf_superuser_role_editor" {
  project = var.project_id
  role = "roles/owner"
  member = "serviceAccount:${google_service_account.tf_superuser.email}"
}

resource "google_project_iam_member" "tf_superuser_role_iam_serviceAccountAdmin" {
  project = var.project_id
  role = "roles/iam.serviceAccountAdmin"
  member = "serviceAccount:${google_service_account.tf_superuser.email}"
}

resource "google_project_iam_member" "tf_superuser_role_resourcemanager_projectIamAdmin" {
  project = var.project_id
  role = "roles/resourcemanager.projectIamAdmin"
  member = "serviceAccount:${google_service_account.tf_superuser.email}"
}

resource "google_project_iam_member" "tf_superuser_role_iam_securityAdmin" {
  project = var.project_id
  role = "roles/iam.securityAdmin"
  member = "serviceAccount:${google_service_account.tf_superuser.email}"
}

resource "google_project_iam_member" "tf_superuser_role_appengine_appAdmin" {
  project = var.project_id
  role = "roles/appengine.appAdmin"
  member = "serviceAccount:${google_service_account.tf_superuser.email}"
}

resource "google_project_iam_member" "tf_superuser_role_serviceusage_serviceUsageAdmin" {
  project = var.project_id
  role = "roles/serviceusage.serviceUsageAdmin"
  member = "serviceAccount:${google_service_account.tf_superuser.email}"
}

resource "google_project_iam_member" "tf_superuser_role_iam_roleAdmin" {
  project = var.project_id
  role = "roles/iam.roleAdmin"
  member = "serviceAccount:${google_service_account.tf_superuser.email}"
}

resource "google_project_iam_member" "tf_superuser_role_storage_admin" {
  project = var.project_id
  role = "roles/storage.admin"
  member = "serviceAccount:${google_service_account.tf_superuser.email}"
}

resource "google_project_iam_member" "tf_superuser_role_compute_networkAdmin" {
  project = var.project_id
  role = "roles/compute.networkAdmin"
  member = "serviceAccount:${google_service_account.tf_superuser.email}"
}

resource "google_project_iam_member" "tf_superuser_role_iam_serviceAccountKeyAdmin" {
  project = var.project_id
  role = "roles/iam.serviceAccountKeyAdmin"
  member = "serviceAccount:${google_service_account.tf_superuser.email}"
}
