.PHONY: help

help:
	@echo "See the sources"

setup-project:
	$(MAKE) -C devops/tf-project setup-project

setup-project-enable-backend:
	$(MAKE) -C devops/tf-project enable-backend
	$(MAKE) -C devops/tf-project setup-project

devops-init-all:
	$(MAKE) -C devops/tf-bootstrap init
	$(MAKE) -C devops/tf-gcp/network init
	$(MAKE) -C devops/tf-gcp/appengine-application init
	$(MAKE) -C devops/tf-gcp/appengine init
	$(MAKE) -C devops/tf-gitlab init

devops-apply-all:
	$(MAKE) -C devops/tf-bootstrap apply
	$(MAKE) -C devops/tf-gcp/network apply
	$(MAKE) -C devops/tf-gcp/appengine-application apply
	$(MAKE) -C devops/tf-gcp/appengine apply
	$(MAKE) -C devops/tf-gitlab apply
