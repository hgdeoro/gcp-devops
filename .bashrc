if [ -e .config/gcp-project.json ] ; then
# 	export CLOUDSDK_ACTIVE_CONFIG_NAME=$(jq -r .projectId < .config/gcp-project.json)
	export CLOUDSDK_CORE_PROJECT=$(jq -r .projectId < .config/gcp-project.json)
# 	echo "Configuring: CLOUDSDK_ACTIVE_CONFIG_NAME=$CLOUDSDK_ACTIVE_CONFIG_NAME"
	echo "Configuring: CLOUDSDK_CORE_PROJECT=$CLOUDSDK_CORE_PROJECT"
fi

echo "python: $(which python)"
echo "gcloud config get-value account: $(gcloud config get-value account 2> /dev/null || true)"
echo ""

git status --short
