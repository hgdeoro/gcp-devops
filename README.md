# DevOps on GCP

## What's here

* terraform modules to deploy backend service to GCP (see `/devops/`)
* Makefile to handle bootstrap of GCP project & Service Accounts (see `/Makefile`)

This is part of a multi-project idea:

* [gcp-devops](https://gitlab.com/hgdeoro/gcp-devops).
* [gcp-app-django-monolithic](https://gitlab.com/hgdeoro/gcp-app-django-monolithic).

### In the roadmap

* use of all available GCP tools for deploying a secure, well architected
    backend service

### Not included at the moment

* Database for the backend service
* Test cases
* Frontend


# Step by step guide

## 1. Initial creation of the project and privileged service account

    $ cp devops/conf/locals.tfvars.txt devops/conf/locals.tfvars
	$ cp devops/conf/variables-globals.tfvars.txt devops/conf/variables-globals.tfvars
    $ make -C devops/tf-project setup-project
    $ make -C devops/tf-project enable-backend

Running `setup-project` will:
* Create project
* Enable services
* Create superuser Service Account and key (with role/owner)
* Create bucket for Terraform state
* Configure Terraform backend

## 2. Apply all other modules

    $ make -C devops/path/to/module init apply

## TODO / FIXMEs

* Migrate `tf-gitlab` (now it's broken)
* Default region to use for Bucket, AppEngine, etc. should be configurable
* Permission & service accounts
    * assume role, instead of use of Service Account keys
    * [google_service_account_access_token](https://www.terraform.io/docs/providers/google/d/datasource_google_service_account_access_token.html)
    * use google_service_account_access_token to give developers access
        to DEV environment
* Make easier to configure GitLab **Personal Access Token**.

# Docs and References

* GCP
   * [AppEngine + Python3 + Django](https://github.com/GoogleCloudPlatform/python-docs-samples/tree/master/appengine/standard_python37/django)
* Terraform
   * [GCP provider](https://www.terraform.io/docs/providers/google/index.html)   
   * [GitLab provider](https://www.terraform.io/docs/providers/gitlab/index.html)   

# Known issues

* GCP Service Accounts name should contain some random characters
    (to avoid problems if the SA needs to be deleted and re-created).
    See details [here](https://cloud.google.com/iam/docs/understanding-service-accounts).

# Looking for inspiration?

* [GitLab.com Automated Infrastructure Resources](https://ops.gitlab.net/gitlab-com/gitlab-com-infrastructure)

# Debug hints

* **gcloud**: use `gcloud --log-http --verbosity=debug`
* **terraform**: set `TF_LOG=debug` env variable
* **make**: use `make -d`
